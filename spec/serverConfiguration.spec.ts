import { assert } from 'chai';
import {ServerConfiguration} from '../src/entities/serverConfiguration';
import {ServerStatus} from '../src/interfaces/serverStatus';


describe('ServerConfiguration', () => {
    it('Serialization', async () => {
        const config = new ServerConfiguration({id: 'id', name: 'name',port: 'port',image: 'image', status: ServerStatus.DOWN, description:'description'});
        
        assert.equal(config.id, 'id');
        assert.equal(config.name, 'name');
        assert.equal(config.port, 'port');
        assert.equal(config.image, 'image');
    });

    it('Serialization from JSON', () => {
        const json = { id:'id', name: 'name', port: 'port', image: 'image', anothervar: 'anothervar', status: ServerStatus.DOWN };
        const config = ServerConfiguration.fromAny(json);

        assert.equal(config.id, 'id');
        assert.equal(config.name, 'name');
        assert.equal(config.port, 'port');
        assert.equal(config.image, 'image');
        assert.isUndefined(config['anothervar']);
    });

});