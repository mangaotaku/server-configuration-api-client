import { IServerConfiguration } from "../interfaces/iServerConfiguration";
import { IServerApiClient } from "../interfaces/iServerApiClient";
import { RestClient } from 'typed-rest-client';
import { Config } from '../Config';

export class ServerApiClient implements IServerApiClient {
    constructor(private apiClient: RestClient = new RestClient(Config.USER_AGENT, Config.CLIENT_ENDPOINT)){}

    async add(config: IServerConfiguration): Promise<string> {
        const response = await this.apiClient.create<string>('/v1/servers', config);
        return response.result;
    }

    async delete(id: string): Promise<void> {
        await this.apiClient.del<void>(`/v1/servers/${id}`);
        return;
    }

    async update(config: IServerConfiguration): Promise<void> {
        await this.apiClient.update<void>('/v1/servers', config);
        return;
    }

    async get(): Promise<IServerConfiguration[]> {
        const response = await this.apiClient.get<IServerConfiguration[]>('/v1/servers');
        return response.result;
    }

    async getOne(id: string): Promise<IServerConfiguration> {
        const response = await this.apiClient.get<IServerConfiguration>(`/v1/servers/${id}`);
        return response.result;
    }
}