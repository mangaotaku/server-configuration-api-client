import {IServerConfiguration} from "../interfaces/iServerConfiguration";
import {ServerStatus} from "../interfaces/serverStatus";

export class ServerConfiguration implements IServerConfiguration {
    id: string;
    name: string;
    port: string;
    image: string;
    status: ServerStatus;
    description:string;

    constructor(config: IServerConfiguration){
        this.id = config.id;
        this.name = config.name;
        this.image = config.image;
        this.port = config.port;
        this.status = config.status;
        this.description = config.description;
    }

    static fromAny(obj: any){
        return new ServerConfiguration({...obj});
    }
}