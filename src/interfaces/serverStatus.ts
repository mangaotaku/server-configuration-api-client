export enum ServerStatus {
    STARTING = 'starting',
    UP = 'up',
    SHUTTING_DOWN = 'shutting_down',
    DOWN = 'down'
}