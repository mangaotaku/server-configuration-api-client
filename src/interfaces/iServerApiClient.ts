import {IServerConfiguration} from "./iServerConfiguration";

export interface IServerApiClient {
    add(config: IServerConfiguration): Promise<string>;
    delete(id: string): Promise<void>;
    update(config: IServerConfiguration): Promise<void>;
    get(): Promise<IServerConfiguration[]>;
    getOne(id: string): Promise<IServerConfiguration>;
}