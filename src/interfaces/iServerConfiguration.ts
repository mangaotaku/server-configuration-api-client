import {ServerStatus} from "./serverStatus";

export interface IServerConfiguration {
    id: string;
    name: string;
    port: string;
    description:string;
    image: string;
    status: ServerStatus;
}