export class Config {
    public static readonly CLIENT_ENDPOINT = process.env.CLIENT_ENDPOINT || 'http://localhost:8082';
    public static readonly USER_AGENT = process.env.USER_AGENT || 'server-configuration-api-client';
}